data "terraform_remote_state" "remote_vars" {
  backend = "s3"
  config = {
    bucket = "ryanimgmgr-common-tf-state-terraformstatebucket-1lq2f6aj1yxgh"
    region = "${var.region}"
    key    = "env:/common/sampleapp.tfstate"
  }
}
