output "alb_url" {
  value = aws_lb.ImgMgrALB.dns_name
}

output "cf_dns_name" {
  value = aws_cloudfront_distribution.distribution.domain_name
}
