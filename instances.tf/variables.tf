variable "region" {
}

variable "environment" {
  description = "The Deployment environment"
}

variable "cw_high" {
  description = "High CPU Threshold"
}

variable "cw_low" {
  description = "Low CPU Threshold"
}

variable "key_name" {
  description = "The SSH Key Pair to be used"
}

variable "bucket_name" {
  description = "s3 bucket"
}

variable "instance_type" {
  description = "The size of ec2 instance"
}

variable "asg_desired_capacity" {
  description = "Autoscaling desired capacity"
}

variable "asg_max_size" {
  description = "Autoscaling maximum capacity"
}

variable "asg_min_size" {
  description = "Autoscaling minimum capacity"
}
