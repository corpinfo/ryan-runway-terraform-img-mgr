# Generate backend bucket/DynamoDB table if applicable

edit sampleapp.cfn/common-us-east-2.env
---------------------------------------

set:  


1. namespace

2. customer

3. environment

4. region

5. cfngin_bucket_name

run
===

``` bash
make deploy common
```

select sampleapp.cfn

Document the

* DynamoDB table name

* S3 Bucket name

# Create VPC

edit vpc.tf/backend-us-east-2.tfvars
------------------------------------

set:  


bucket and dynamodb_table are from generated sampleapp.cfn

  *  bucket

  *  region

  *  dynamodb_table

  *  edit vpc.tf/common-us-east-2.tfvars

edit vpc.tf/common-us-east-2.tfvars
-----------------------------------

set:  


1. region

2. vpc_cidr to /16, this can be modified in vpc.tf/main under aws_subnet, it currently dynamically generates /24 within your /16

3. public_subnets_count

4. private_subnets_count

run
===

``` bash
make deploy-common

```

select vpc.cfn

# Create img-mgr

edit instances.tf/backend-us-east-2.tfvars
------------------------------------------

set:   


bucket and dynamodb_table are from generated sampleapp.cfn

* bucket

* region

* dynamodb_table

edit instances.tf/data_import.tf
--------------------------------

set:   


bucket is from generated sampleapp.cfn

* bucket


edit instances.tf/dev-us-east-2.tfvars and instances.tf/prod-us-east-2.tfvars
-----------------------------------------------------------------------------

set:  


1. region

2. environment = dev/prod which ever you are producing

3. key_name    = the key you have added to aws console

4. instance_type = "t2.micro"

5. bucket_name = the bucket name you want for img-mgr

4. run

``` bash
git checkout -b ENV-prod or git checkout -b ENV-dev

runway deploy

```

# default region is us-east-2

this is coded in

*  runway.yml

*  sampleapp.cfn/common-us-east-2.env

*  instances.tf/backend-us-east-2.tfvars

*  instances.tf/dev-us-east-2.tfvars

*  instances.tf/prod-us-east-2.tfvars

(you must also change the filename) if the filename includes the region
