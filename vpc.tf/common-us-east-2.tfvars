region      = "us-east-2"
environment = "dev"
#key_name    = "my_key"
#bucket_name = "img-mgr-tf-bucket-east2"
bucket_name = "cfngin-ryanimgmgr-us-east-2"

vpc_cidr             = "10.0.0.0/16"
public_subnets_count = 2
private_subnets_count = 2
azs_count             = 2
